#!/bin/bash

# Script intended to be ran in a shell function to determine if a path
# is fscrypt'ed and if so, unlock it and create a shell there.  If the
# path doesn't exist it offers to create it.  This script, and the
# function below, can be used to entirely "replace" cd

# Uses fscrypt from https://github.com/google/fscrypt
# Single binary install:  go get github.com/google/fscrypt/cmd/fscrypt

# If you want your prompt or some other shell values changed while
# cd'ed into a crypt'ed path, in your rc files look for FSCRYPT=1
# Example: 
# Set FSCRYPT in script to change prompt in subshell
#if [[ $FSCRYPT -gt 0 ]]; then
#    PROMPT="%{$bg[red]$fg_bold[cyan]%}%n%{$reset_color$bg[red]%}@%{$fg_bold[green]%}%m%{$reset_color%} %T [%.]%# "
#fi

# To create a function wrapper to replace cd, put this in your
# .zshrc or .bashrc
# It handles "cd" by itself, handles returning to the previous directory
# based on exit status of script, etc, so the behavior hopefully makes
# sense
# 
#function cd {
#    if [[ "$@" == "" ]]; then builtin cd; else oldpwd=`pwd` newpath=`realpath "$@"` && builtin cd "$@" ; ~/bin/cryptcd-funct.sh "${newpath}" && builtin cd "${oldpwd}"; fi
#}

# When the script exits using the function, if the status code is 0 it
# will automaticaly go to the previous directory, otherwise it will
# stay in the current directory

# Adjust path if needed
fscrypt="${HOME}/bin/go/bin/fscrypt"

# Attempt to create encrypted directory if it does not exist?
makedir="1"

# If no fscrypt, just bail out
if [ ! -x "${fscrypt}" ]; then
    echo "${fscrypt} not found; not attempting fscrypt logic"
    exit
fi

path=$1

# Loop to unlock in the background, notify every 30s if unable to relock
unlock ()
{
    # Get out script out of this directory so we can lock it
    # Does not affect what directory the shell itself is in
    builtin cd /tmp

    # Set rc to something non-zero for our loop
    rc=99999

    # Locked counter
    LOCKED=0

    # Track if we echoed a failure message
    ECHOED=0
    
    # How many times to loop before notifying, sleep 1 sec each loop
    LOOPS=30

    # Loop forever until rc=0
    while [ ${rc} -ne 0 ]; do
        sync
        ${fscrypt} lock "${path}" &> /dev/null
        rc=$?
        # If not 0, relock failed
        if [ $rc -gt 0 ]; then
            # If it's actually locked already it'll fail with rc=1, same
            # as if it failed to lock, so check the actuall lock status
            status=`${fscrypt} status "${path}" | grep "Unlocked:" | cut -f 2 -d " "`
            if [ "${status}" == "No" ]; then
                rc=0
            fi
            # Echo a warning every LOOPS seconds or so if necessary
            if [ ${LOCKED} -gt ${LOOPS} ]; then
                echo ""
                echo "fscrypt lock failed, looping until locked.  rc: [$rc]"
                # Reset for another LOOPS seconds
                LOCKED=0
                # Set so we show final echo when lock succeeds
                ECHOED=1
            fi
            ((LOCKED++))
            sleep 1s
        fi
    done

    # Show final status if we echoed a failure
    if [ ${ECHOED} -ne 0 ]; then
        echo "fscrypt locked again"
    fi
}

# Logic to create and/or enter locked directory

# If path exists, see if it's a fscrypt directory
if [ -d "${path}" ]; then
    builtin cd "${path}"
    ${fscrypt} status "${path}" >/dev/null 2>&1
    rc=$?

    # 0 means fscrypt path
    if [ ${rc} == 0 ]; then
        status=`${fscrypt} status "${path}" | grep "Unlocked:" | cut -f 2 -d " "`
        if [ "${status}" == "Yes" ]; then
            # Already in a decrypted directory, nothing to do, but
            # exit with status 1 so we stay in this directory
            exit 1
        fi
        if [ "${status}" == "No" ]; then
            echo "Decrypting directory.  Press ^D to exit."
            ${fscrypt} unlock "${path}"
            # If we don't succeed, just exit with status 0
            rc=$?
            if [ ${rc} != 0 ]; then
                exit 0
            fi
            FSCRYPT=1 $SHELL
            ( unlock "${path}" ) &
        fi
    else
        # Exit with 1 so we stay in this directory
        exit 1
    fi
# Path does not exist, create and fscrypt it
else
    if [ ${makedir} == "1" ]; then
        echo -n "Path does not exist, create and encrypt? (y/N) "
        read resp
        if [ "${resp}" == "y" ]; then
            mkdir "${path}"
            rc=$?
            if [ ${rc} == 0 ]; then
                builtin cd "${path}"
                ${fscrypt} encrypt "${path}"
                echo "Created new encrypted directory.  Press ^D to exit."
                FSCRYPT=1 $SHELL
                ( unlock "${path}" ) &
            else
                exit 1
            fi
        else
            echo "Aborting; no directory created.."
        fi
    fi
    exit 1
fi
