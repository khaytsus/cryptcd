#!/bin/bash

# Script to determine if a path is fscrypt'ed and if so, unlock it
# and create a shell there.  If the path doesn't exist it offers to
# create it.  This script is intended to be ran standalone to manually
# enter or create encrypted directories, but otherwise just use cd to
# move around once inside of them or in any other path.

# Uses fscrypt from https://github.com/google/fscrypt
# Single binary install:  go get github.com/google/fscrypt/cmd/fscrypt

# If you want your prompt or some other shell values changed while
# cd'ed into a crypt'ed path, in your rc files look for FSCRYPT=1
# Example: if [ "$FSCRYPT" -eq "1" ]; then PROMPT="LOCKED " fi

# Adjust path if needed
fscrypt="${HOME}/bin/go/bin/fscrypt"

# Attempt to create encrypted directory if it does not exist?
makedir="1"

# If no fscrypt, just bail out
if [ ! -x "${fscrypt}" ]; then
    echo "${fscrypt} not found; not attempting fscrypt logic"
    exit
fi

path=$1

if [ "${path}" == "" ]; then
    echo "$0 [path]"
    exit
fi

# Loop to unlock in the background, notify every 30s if unable to relock
unlock ()
{
    # Get out script out of this directory so we can lock it
    # Does not affect what directory the shell itself is in
    builtin cd /tmp

    # Set rc to something non-zero for our loop
    rc=99999

    # Locked counter
    LOCKED=0

    # Track if we echoed a failure message
    ECHOED=0
    
    # How many times to loop before notifying, sleep 1 sec each loop
    LOOPS=30

    # Loop forever until rc=0
    while [ ${rc} -ne 0 ]; do
        path=$*
        sync
        ${fscrypt} lock "${path}" &> /dev/null
        rc=$?
        # If not 0, relock failed
        if [ $rc -gt 0 ]; then
            # If it's actually locked already it'll fail with rc=1, same
            # as if it failed to lock, so check the actuall lock status
            status=`${fscrypt} status "${path}" | grep "Unlocked:" | cut -f 2 -d " "`
            if [ "${status}" == "No" ]; then
                rc=0
            fi
            # Echo a warning every LOOPS seconds or so if necessary
            if [ ${LOCKED} -gt ${LOOPS} ]; then
                echo ""
                echo "fscrypt lock failed, looping until locked.  rc: [$rc]"
                # Reset for another LOOPS seconds
                LOCKED=0
                # Set so we show final echo when lock succeeds
                ECHOED=1
            fi
            ((LOCKED++))
            sleep 1s
        fi
    done

    # Show final status if we echoed a failure
    if [ ${ECHOED} -ne 0 ]; then
        echo "fscrypt locked again"
    fi
}

# Logic to create and/or enter locked directory

# If path exists, see if it's a fscrypt directory
if [ -d ${path} ]; then
    pwd=`pwd`
    cd "${path}"
    newpwd=`pwd`
    ${fscrypt} status . >/dev/null 2>&1
    rc=$?
    # 0 means fscrypt path
    if [ ${rc} == 0 ]; then
        status=`${fscrypt} status . | grep "Unlocked:" | cut -f 2 -d " "`
        if [ "${status}" == "Yes" ]; then
            # echo "Already decrypted, nothing to do.  Press ^D to exit."
            # FSCRYPT=1 $SHELL
            # cd "${pwd}"
            # ( unlock "${newpwd}" ) &
            exit
        fi
        if [ "${status}" == "No" ]; then
            echo "Decrypting directory.  Press ^D to exit."
            ${fscrypt} unlock "${newpwd}"
            rc=$?
            if [ ${rc} != 0 ]; then
                exit 0
            fi
            FSCRYPT=1 $SHELL
            cd "${pwd}"
            ( unlock "${newpwd}" ) &
        fi
    else
        echo "Path not fscrypt'ed; use normal cd"
        exit 0
    fi
# Path does not exist, create and fscrypt it
else
    if [ ${makedir} == "1" ]; then
        echo -n "Path does not exist, create and encrypt? (y/N) "
        read resp
        if [ "${resp}" == "y" ]; then
            mkdir ${path}
            rc=$?
            if [ ${rc} == 0 ]; then
                cd "${path}"
                newpwd=`pwd`
                ${fscrypt} encrypt "${newpwd}"
                echo "Created directory.  Press ^D to exit."
                FSCRYPT=1 $SHELL
                cd "${pwd}"
                ( unlock "${newpwd}" ) &
            else
                echo "Aborting; no directory created.."
            fi
        fi
    fi
    exit
fi
