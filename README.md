# cryptcd

While is it generally syggested to use full disk encryption on most modern systems, there are certain things you may want to encrypt separately or perhaps put on something not as easily encrypted, such as a portable drive.  [Filesystem encryption built into Linux filesystems](https://www.kernel.org/doc/html/latest/filesystems/fscrypt.html) make this trival to do using [fscrypt](https://github.com/google/fscrypt) and can create an encrypted directory at any time on any supported filesystem.

This project makes it even easier to use by automatically detecting an fscrypt'ed directory and prompting to unlock it when you cd into it.  It also by default will offer to make a new encrypted directory if the directory you're cd'ing into does not already exist.  

This project has two script files that are both very similar, but their use cases are slightly different and are described below.

## Common traits of both scripts

Both scripts will detect the encrypted directory, prompt to unlock them, and then create a subshell once unlocking has happened.  While that subshell is active the directory remains unencrypted and you can do any normal operations on files, directories, etc.  When you're done, exit the subshell (using the exit command, or ^d) and they will drop you to your last used directory and automatically lock the encrypted directory.  If it is unable to do so for some reason, it will continue to retry and will notify you that it is not been successful.

This should make usage of encrypted directories quite seamless and safe, as when you're done you don't have to worry about locking them back, etc.

When in the subshell, you can use the FSCRYPT env variable to give you a hint that you're still in the subshell and your directory is still unlocked.  Here is an example of changing the prompt:

    # Set FSCRYPT in script to change prompt in subshell
    if [[ $FSCRYPT -gt 0 ]]; then
        PROMPT="%{$bg[red]$fg_bold[cyan]%}%n%{$reset_color$bg[red]%}@%{$fg_bold[green]%}%m%{$reset_color%} %T [%.]%# "
    fi

The only real option both scripts offer is the ability to create a directory if it does not already exist, which is set at the top of the script.  If this is enabled and you attempt to cd into a directory which does not exist it will prompt to create and encrypt it.  If unset, the cd will simply "fail" like any normal cd into a directory which does not exist.

You also need to make sure the fscrypt variable points to the fscrypt binary.

## cryptcd.sh

This is a script intended to be manually executed by the user like any typical shell script, typically only when intending to enter or create an encrypted directory.  Example:  cryptcd.sh ~/encrypted

## cryptcd-funct.sh

This shell script is really intended to be executed from a function in your shell, such as the ability to make it executed by default when you use the "cd" command, making its usage completely transparent.  If the directory is encrypted, it will prompt to unlock it.  If it's not encrypted it simply cd's into it like normal.

This is intended to be absolutely transparent to the user.

To make this work, you need to add a function to your .zsh or .bashrc (other shells can likely work as well, I may add more examples in the future)

    function cd {
        if [[ "$@" == "" ]]; then builtin cd; else oldpwd=`pwd` newpath=`realpath "$@"` && builtin cd "$@" ; ~/bin/cryptcd-funct.sh "${newpath}" && builtin cd "${oldpwd}"; fi
    }

This is carefully crafted so that it can return to the original path, but based on the exit code of the script it will or will not return to the previous directory.  If there is some failure/error in the script, dropping back to the previous directory makes sense, but if it succeeds to unlock it will of course remain in the target directory.  It also handles an empty "cd" to just return to your homedir, etc.

## Limitations

If the directory or a file in the directory are still in use in any way, the script can not lock the directory again.  This should not happen in typical use, but there may be times you will need to manualy lock your directory.  Most cases, however, the script will lock it for you and continue to warn you that it was unable to do so as long as the parent shell is still running.

Not directly related to these scripts, but you can not copy or backup a locked directory so your back application may encounter errors when attempting to back up an encrypted and locked directory.  Hopefully they do not treat these as fatal errors and simply ignore the errors and complete the rest of your backups but I would highly suggest validating that.  I use restic and it reports a return code of 3 indicating it did not succed to back up all files but it otherwise completes the rest of the backup.  You can potentially exclude your encrypted directories from the backup.  All of this also implies that if you need to back up your fscrypt encrypted directories and files you will need to do this with them unlocked or in some other manner.

## Installation

git clone the directory and either place or symlink the files into your path.  If you want to use cyrptcd-funct.sh you'll need to add the function into your shell scripts.

These scripts are very safe, however, I would suggest initial testing in /tmp or some other temporary or unimportant location.  fscrypt can not encrypt a directory with any files in it so there is no danger in it encrypting files you do not expect it to.

## Requirements

You need to use a supported filesystem which are listed on the [Filesystem encryption built into Linux filesystems](https://www.kernel.org/doc/html/latest/filesystems/fscrypt.html) page.

You also need to have [fscrypt](https://github.com/google/fscrypt) built and installed on your system and the scripts pointing to the correct path for it.  This script is really just a wrapper around fscrypt, but I do suggest reading the fscrypt webpage and info and understanding what it does, how it works, etc.

## License

[GPL 3](LICENSE)